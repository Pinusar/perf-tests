#bin/bash

bzt \
  -o settings.env.SERVER_URL=$SERVER_URL \
  -o settings.env.CONCURRENCY=$CONCURRENCY \
  -o settings.env.RAMP_UP=$RAMP_UP \
  -o settings.env.HOLD_FOR=$HOLD_FOR \
  $1