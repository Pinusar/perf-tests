# Stress tests for tehik project

This repository contains jmeter stress tests. The tests are run using Taurus, which is a wrapper for Jmeter.

## Running the tests

### Prerequisites

You have Taurus installed.

### Running with Taurus

Please note it's necessary to set the following env variables.

* SERVER_URL
* CONCURRENCY
* RAMP_UP
* HOLD_FOR

For getting notifications:
```
bzt \
  -o settings.env.SERVER_URL=$SERVER_URL \
  -o settings.env.CONCURRENCY=$CONCURRENCY \
  -o settings.env.RAMP_UP=$RAMP_UP \
  -o settings.env.HOLD_FOR=$HOLD_FOR \
  get-notifications.yml
```

For inserting notifications:
```
bzt \
  -o settings.env.SERVER_URL=$SERVER_URL \
  -o settings.env.CONCURRENCY=$CONCURRENCY \
  -o settings.env.RAMP_UP=$RAMP_UP \
  -o settings.env.HOLD_FOR=$HOLD_FOR \
  insert-notification.yml
```
